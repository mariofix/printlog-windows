@echo off
:Esto crea un timestamp para no crear un mismo archivo
SET HOUR=%time:~0,2%
SET dtStamp9=%date:~-4%%date:~4,2%%date:~7,2%_0%time:~1,1%%time:~3,2%%time:~6,2% 
SET dtStamp24=%date:~-4%%date:~4,2%%date:~7,2%_%time:~0,2%%time:~3,2%%time:~6,2%
if "%HOUR:~0,1%" == " " (SET dtStamp=%dtStamp9%) else (SET dtStamp=%dtStamp24%)

SET archivo=c:\impresiones\%dtStamp%.log
SET archivo_log=c:\impresiones\print.log

:Va a buscar el último 307 de PrintService
wevtutil qe Microsoft-Windows-PrintService/Operational /q:*[System[(EventID=307)]] /c:1 /rd:true >> %archivo%

:Va a buscar el último 805 de PrintService
echo ----- >> %archivo%
wevtutil qe Microsoft-Windows-PrintService/Operational /q:*[System[(EventID=805)]] /c:1 /rd:true >> %archivo%

:Lo envía a intranet para su procesamiento
c:\impresiones\curl.exe -X POST --header "Content-Type:text/xml;charset=UTF-8" --data @%archivo% REPLACE_URL_HERE
echo ----- >> %archivo%

:Registro y eliminación de basura
more %archivo% >> %archivo_log%
del %archivo%
